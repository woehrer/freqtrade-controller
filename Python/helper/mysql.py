from datetime import datetime
import logging
import time

import helper.config
import mysql.connector

conf = helper.config.initconfig()


def init():
    mydb = mysql.connector.connect(
        host = conf['MYSQL_HOST'],
        user = conf['MYSQL_USER'],
        password = conf['MYSQL_PASS'],
    )
    mycursor = mydb.cursor()

    mycursor.execute("CREATE DATABASE IF NOT EXISTS Trading4Live")
    
    mycursor.close()

    mydb = mysql.connector.connect(
    host = conf['MYSQL_HOST'],
    user = conf['MYSQL_USER'],
    password = conf['MYSQL_PASS'],
    database = 'Trading4Live'
    )
    mycursor = mydb.cursor()

    # Users
    mycursor.execute('''CREATE TABLE IF NOT EXISTS Users
                    (Telegramid integer, Firstname text, Level integer, Balance integer)''')
    
    mycursor.close()

# Users
# Insert
def insertuser(Telegramid, Firstname, Level, Balance):
    try:
        mydb = mysql.connector.connect(
        host = conf['MYSQL_HOST'],
        user = conf['MYSQL_USER'],
        password = conf['MYSQL_PASS'],
        database = 'Trading4Live'
        )
        mycursor = mydb.cursor()
        execute = """INSERT INTO Users (Telegramid, Firstname, Level, Balance) VALUES (%s,%s,%s,%s)"""
        query = Telegramid,Firstname, Level, Balance
        mycursor.execute(execute, query)
        mydb.commit()
        mycursor.close()
        return True
    except Exception as e:
        logging.error("Error while writing new User in SQL: " + str(e))
        print("Error while writing new User in SQL: " + str(e))
        return False

# Request
def requestuser(Telegramid):
    try:
        mydb = mysql.connector.connect(
        host = conf['MYSQL_HOST'],
        user = conf['MYSQL_USER'],
        password = conf['MYSQL_PASS'],
        database = 'Trading4Live'
        )
        mycursor = mydb.cursor()
        execute = """SELECT * FROM Users WHERE Telegramid = {}""".format(Telegramid)
        mycursor.execute(execute)
        myresult = mycursor.fetchone()
        mycursor.close()
        return myresult
    except Exception as e:
        logging.error("Error while request Users in SQL: " + str(e))
        print("Error while request Users in SQL: " + str(e))
        return False

def deleteuser(Telegramid):
    try:
        mydb = mysql.connector.connect(
        host = conf['MYSQL_HOST'],
        user = conf['MYSQL_USER'],
        password = conf['MYSQL_PASS'],
        database = 'Trading4Live'
        )
        mycursor = mydb.cursor()
        execute = """DELETE FROM Users WHERE Telegramid = {}""".format(Telegramid)
        mycursor.execute(execute)
        mydb.commit()
        mycursor.close()
        return True
    except Exception as e:
        logging.error("Error while deleting User in SQL: " + str(e))
        print("Error while deleting User in SQL: " + str(e))
        return False
