import logging
import os
import helper.subconfig

conf = helper.config.initconfig()
Token = conf['TELEGRAM_TOKEN']

def initlogger():
	if not os.path.isdir("./logs"):
		os.mkdir("./logs")
	logger = logging.getLogger()
	logger.setLevel(logging.DEBUG)
	handler = logging.FileHandler("logs/mining.log")
	formatter = logging.Formatter('%(asctime)s:%(levelname)s-%(message)s')
	handler.setFormatter(formatter)
	handler.setLevel(logging.DEBUG)
	logger.addHandler(handler)

def create_account(Telegramid):
	try:
		logging.info("create_account")
		if not os.path.isdir("./accounts"):
			os.mkdir("./accounts")
		
		if not os.path.isdir("./accounts/" + str(Telegramid)):
			os.mkdir("./accounts/" + str(Telegramid))
			os.system("git clone https://gitlab.com/woehrer/freqtrade-automat ./accounts/{}".format(Telegramid))
			os.system("cd accounts/{} && python3 Python/automat.py >> ./create.log".format(Telegramid))
			helper.subconfig.insertTelegramid(Telegramid)
			helper.subconfig.insertTelegramtoken(Telegramid, conf['telegram_token'])
			logging.info("Account created")
			return True
		else:
			logging.error("Account already exists")
			return False
	except Exception as e:
		logging.error("Error while creating account: " + str(e))
		print("Error while creating account: " + str(e))
		return False

def start_hyperopting(Telegramid):
	logging.info("start_hyperopting")
	import threading
	prozess = threading.Thread(target=start,args=(Telegramid,))
	prozess.start()

def start(Telegramid):
	os.system("cd accounts/{} && python3 Python/automat.py hyperopting >> ./logs/hyperopting.log".format(Telegramid))