import configparser


config = configparser.ConfigParser()

def insertTelegramid(Telegramid):
    config.read('accounts/' + str(Telegramid) + '/config/config.ini') 
    config.set('DEFAULT','Telegramid',str(Telegramid))
    with open('accounts/' + str(Telegramid) + '/config/config.ini', 'w') as configfile:
        config.write(configfile)

def insertTelegramtoken(Telegramid, Telegramtoken):
    config.read('accounts/' + str(Telegramid) + '/config/config.ini')
    config.set('DEFAULT','telegram_token',(Telegramtoken))
    with open('accounts/' + str(Telegramid) + '/config/config.ini', 'w') as configfile:
        config.write(configfile)

def changestrategy(Telegramid, strategy):
    config.read('accounts/' + str(Telegramid) + '/config/config.ini')
    config.set('DEFAULT','strategy',strategy)
    with open('accounts/' + str(Telegramid) + '/config/config.ini', 'w') as configfile:
        config.write(configfile)

def changehyperopt_loss(Telegramid, hyperopt_loss):
    config.read('accounts/' + str(Telegramid) + '/config/config.ini')
    config.set('DEFAULT','hyperopt_loss',hyperopt_loss)
    with open('accounts/' + str(Telegramid) + '/config/config.ini', 'w') as configfile:
        config.write(configfile)

def changehyperoptepochs(Telegramid, hyperoptepochs):
    config.read('accounts/' + str(Telegramid) + '/config/config.ini')
    config.set('DEFAULT','hyperoptepochs',hyperoptepochs)
    with open('accounts/' + str(Telegramid) + '/config/config.ini', 'w') as configfile:
        config.write(configfile)

def changehyperopt_timeframe(Telegramid, hyperopt_timeframe):
    config.read('accounts/' + str(Telegramid) + '/config/config.ini')
    config.set('DEFAULT','hyperopt_timeframe',hyperopt_timeframe)
    with open('accounts/' + str(Telegramid) + '/config/config.ini', 'w') as configfile:
        config.write(configfile)

def changehyperopt_timerange_days(Telegramid, hyperopt_timerange_days):
    config.read('accounts/' + str(Telegramid) + '/config/config.ini')
    config.set('DEFAULT','hyperopt_timerange_days',hyperopt_timerange_days)
    with open('accounts/' + str(Telegramid) + '/config/config.ini', 'w') as configfile:
        config.write(configfile)