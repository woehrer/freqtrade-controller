#!/usr/bin/env python
# pylint: disable=unused-argument, wrong-import-position
# This program is dedicated to the public domain under the CC0 license.

"""
Simple Bot to reply to Telegram messages.

First, a few handler functions are defined. Then, those functions are passed to
the Application and registered at their respective places.
Then, the bot is started and runs until we press Ctrl-C on the command line.

Usage:
Basic Echobot example, repeats messages.
Press Ctrl-C on the command line or send a signal to the process to stop the
bot.
"""
import helper.config
import helper.mysql
import helper.functions
import helper.subconfig

helper.mysql.init()

import logging

conf = helper.config.initconfig()
Token = conf['telegram_token']

from typing import Dict

from telegram import __version__ as TG_VER

try:
    from telegram import __version_info__
except ImportError:
    __version_info__ = (0, 0, 0, 0, 0)  # type: ignore[assignment]

if __version_info__ < (20, 0, 0, "alpha", 1):
    raise RuntimeError(
        f"This example is not compatible with your current PTB version {TG_VER}. To view the "
        f"{TG_VER} version of this example, "
        f"visit https://docs.python-telegram-bot.org/en/v{TG_VER}/examples.html"
    )
from telegram import ReplyKeyboardMarkup, ReplyKeyboardRemove, Update, InlineKeyboardButton, InlineKeyboardMarkup
from telegram.ext import (
    Application,
    CommandHandler,
    ContextTypes,
    ConversationHandler,
    MessageHandler,
    filters,
)

# Enable logging
logging.basicConfig(
    format="%(asctime)s - %(name)s - %(levelname)s - %(message)s", level=logging.INFO
)
logger = logging.getLogger(__name__)

START, LOGIN, CONFIG, CHANGE_STRATEGY, CHANGE_HYPEROPT_LOSS, CHANGE_EPOCHS, CHANGE_TIMEFRAME, CHANGE_TIMERANGE_DAYS = range(8)

start_keyboard = [
    ["/bitcoin", "/ethereum"],
    ["/create_account", "/login"],
    ["/cancel","/start"]
]
new_keyboard = [
    ["/start"]]
login_keyboard = [
    ["/account_info","/hyperopting"],
    ["/config"],
    ["/cancel","/start"]
]
config_keyboard = [
    ["/change_strategy"],
    ["/change_hyperopt_loss"],
    ["/change_hyperoptepochs"],
    ["/change_hyperopt_timeframe"],
    ["/change_hyperopt_timerange_days"],
    ["/back"],
    ["/cancel","/start"]
]
strategy_keyboard = [
    ["AverageStrategy"],
    ["BinHV45"],
    ["CofiBitStrategy"],
    ["Diamond"],
    ["GodStraNew"],
    ["HourBasedStrategy"],
    ["mabStra"],
    ["MACDStrategy"],
    ["MultiMa"],
    ["ReinforcedSmoothScalp"],
    ["Supertrend"],
    ["SwingHighToSky"],
    ["wtc"],
    ["/cancel","/start"]
]
hyperopt_loss_keyboard = [
    ["ShortTradeHyperOptLoss"],
    ["OnlyProfitHyperOptLoss"],
    ["SharpeHyperOptLoss"],
    ["SharpeHyperOptLossDaily"],
    ["SortinoHyperOptLoss"],
    ["SortinoHyperOptLossDaily"],
    ["MaxDrawDownHyperOptLoss"],
    ["MaxDrawDownRelativeHyperOptLoss"],
    ["CalmarHyperOptLoss"],
    ["ProfitDrawDownHyperOptLoss"]
]
epoch_keyboard = [
    ["10"],
    ["50"],
    ["100"],
    ["500"],
    ["1000"],
]
timeframe_keyboard = [
    ["1m"],
    ["5m"],
    ["15m"],
    ["1h"],
    ["4h"],
    ["1d"],
]
timerange_keyboard = [
    ["100"],
    ["200"],
    ["500"],
    ["1000"],
]


markup = ReplyKeyboardMarkup(start_keyboard)

async def start(update: Update, context: ContextTypes.DEFAULT_TYPE) -> int:
    """Start the conversation and ask user for input."""
    user = update.message.from_user
    await update.message.reply_text(
        "Hi {}! Welcome to Trading4Live.\n" 
        "Your User ID is: {}\n"
        "send Bitcoin 0,001 to: \n'TODO Hier Bitcoin Adresse eintragen' or \n"
        "send Ethereum 0,0001 to: \n 'TODO Hier Ethereum Adresse eintragen'\n"
        "for registration".format(user['first_name'], user['id']),
        reply_markup=ReplyKeyboardMarkup(start_keyboard),
    )
    return START


async def bitcoin(update: Update, context: ContextTypes.DEFAULT_TYPE) -> int:

    await update.message.reply_text(
        "Tutorial to send Bitcoin: \n"
        "1. Open your Bitcoin wallet\n"
        "2. Select the 'Send' tab\n"
        "3. Enter the address: 'TODO Hier Bitcoin Adresse eintragen'\n"
        "4. Enter the amount\n"
        "5. Click 'Send'\n"
        "6. Wait for the transaction to be confirmed\n"
        "7. Check your balance",
        reply_markup=ReplyKeyboardMarkup(start_keyboard),
    )
    return START

async def ethereum(update: Update, context: ContextTypes.DEFAULT_TYPE) -> int:

    await update.message.reply_text(
        "Tutorial to send Ethereum: \n"
        "1. Open your Ethereum wallet\n"
        "2. Select the 'Send' tab\n"
        "3. Enter the address: 'TODO Hier Ethereum Adresse eintragen'\n"
        "4. Enter the amount\n"
        "5. Click 'Send'\n"
        "6. Wait for the transaction to be confirmed\n"
        "7. Check your balance",
        reply_markup=ReplyKeyboardMarkup(start_keyboard),
    )
    return START

async def create_account(update: Update, context: ContextTypes.DEFAULT_TYPE) -> int:
    # TODO not create via telegram
    check = helper.mysql.requestuser(update.message.from_user.id)
    if check == None:
        # TODO Delete the next line
        checkcreate = helper.mysql.insertuser(update.message.from_user.id,update.message.from_user.first_name,0,0)
        if checkcreate == True:
            check2 = helper.functions.create_account(update.message.from_user.id)
            if checkcreate == True and check2 == True:
                await update.message.reply_text("Account created successfully!\n DAS IST EIN PROVISORISCHER TEST",
                    reply_markup=ReplyKeyboardMarkup(login_keyboard))
                return LOGIN
            else:
                await update.message.reply_text("Account not created!\n Instance")
                return START
        else:
            await update.message.reply_text("Account not created!\n Database")
            return START

    # Account alredy exist
    else:
        await update.message.reply_text("Account already exist!",
            reply_markup=ReplyKeyboardMarkup(login_keyboard))
        return LOGIN

async def login(update: Update, context: ContextTypes.DEFAULT_TYPE) -> int:
    check = helper.mysql.requestuser(update.message.from_user.id)
    if check == None:
        await update.message.reply_text("Account not exist!")
        return START
    else:
        await update.message.reply_text("You are logged in!\n Start with /config to change your strategy\n then /hyperopting to start the hyperopt\n for search the best strategy",
            reply_markup=ReplyKeyboardMarkup(login_keyboard))
        return LOGIN

async def account_info(update: Update, context: ContextTypes.DEFAULT_TYPE) -> int:
    """Ask the user for info about the selected predefined choice."""
    check = helper.mysql.requestuser(update.message.from_user.id)
    await update.message.reply_text("Accound ID: {}\nName: {}\nLevel: {}\nBalance: {}".format(check[0],check[1],check[2],check[3]),)

    return LOGIN

async def hyperopting(update: Update, context: ContextTypes.DEFAULT_TYPE) -> int:
    helper.functions.start_hyperopting(update.message.from_user.id)
    await update.message.reply_text("Hyperopting started!",
        reply_markup=ReplyKeyboardMarkup(login_keyboard))
    return LOGIN

async def config(update: Update, context: ContextTypes.DEFAULT_TYPE) -> int:
    await update.message.reply_text("Config", reply_markup=ReplyKeyboardMarkup(config_keyboard))
    return CONFIG

async def change_strategy(update: Update, context: ContextTypes.DEFAULT_TYPE) -> int:
    #TODO insert all strategys
    await update.message.reply_text("Change strategy",
        reply_markup=ReplyKeyboardMarkup(strategy_keyboard))
    return CHANGE_STRATEGY

async def change_strategy2(update: Update, context: ContextTypes.DEFAULT_TYPE) -> int:
    helper.subconfig.changestrategy(update.message.from_user.id,update.message.text)
    await update.message.reply_text("Change strategy to {}".format(update.message.text),
        reply_markup=ReplyKeyboardMarkup(config_keyboard))
    return CONFIG

async def change_hyperopt_loss(update: Update, context: ContextTypes.DEFAULT_TYPE) -> int:
    await update.message.reply_text("Change Hyperopt Loss Function",
        reply_markup=ReplyKeyboardMarkup(hyperopt_loss_keyboard))
    return CHANGE_HYPEROPT_LOSS

async def change_hyperopt_loss2(update: Update, context: ContextTypes.DEFAULT_TYPE) -> int:
    helper.subconfig.changehyperopt_loss(update.message.from_user.id,update.message.text)
    await update.message.reply_text("Change Hyperopt Loss to {}".format(update.message.text),
        reply_markup=ReplyKeyboardMarkup(config_keyboard))
    return CONFIG

async def change_hyperoptepochs(update: Update, context: ContextTypes.DEFAULT_TYPE) -> int:
    await update.message.reply_text("Change Hyperopt Epochs!",
        reply_markup=ReplyKeyboardMarkup(epoch_keyboard))
    return CHANGE_EPOCHS

async def change_hyperoptepochs2(update: Update, context: ContextTypes.DEFAULT_TYPE) -> int:
    helper.subconfig.changehyperoptepochs(update.message.from_user.id,update.message.text)
    await update.message.reply_text("Change Epochs to {}".format(update.message.text),
        reply_markup=ReplyKeyboardMarkup(config_keyboard))
    return CONFIG

async def change_hyperopt_timeframe(update: Update, context: ContextTypes.DEFAULT_TYPE) -> int:
    await update.message.reply_text("Change Timeframe",
        reply_markup=ReplyKeyboardMarkup(timeframe_keyboard))
    return CHANGE_TIMEFRAME

async def change_hyperopt_timeframe2(update: Update, context: ContextTypes.DEFAULT_TYPE) -> int:
    helper.subconfig.changehyperopt_timeframe(update.message.from_user.id,update.message.text)
    await update.message.reply_text("Change Timeframe to {}".format(update.message.text),
        reply_markup=ReplyKeyboardMarkup(config_keyboard))
    return CONFIG

async def change_hyperopt_timerange_days(update: Update, context: ContextTypes.DEFAULT_TYPE) -> int:
    await update.message.reply_text("Change Timerange Days",
        reply_markup=ReplyKeyboardMarkup(timerange_keyboard))
    return CHANGE_TIMERANGE_DAYS

async def change_hyperopt_timerange2(update: Update, context: ContextTypes.DEFAULT_TYPE) -> int:
    helper.subconfig.changehyperopt_timerange_days(update.message.from_user.id,update.message.text)
    await update.message.reply_text("Change Timerange Days to {}".format(update.message.text),
        reply_markup=ReplyKeyboardMarkup(config_keyboard))
    return CONFIG

async def back_to_login(update: Update, context: ContextTypes.DEFAULT_TYPE) -> int:
    await update.message.reply_text("Back to Login",
        reply_markup=ReplyKeyboardMarkup(login_keyboard))
    return LOGIN


async def cancel(update: Update, context: ContextTypes.DEFAULT_TYPE) -> int:
    await update.message.reply_text(
        "Byebye!",
        reply_markup=ReplyKeyboardMarkup(new_keyboard),
    )

    return ConversationHandler.END


def main() -> None:
    """Run the bot."""
    # Create the Application and pass it your bot's token.
    application = Application.builder().token(Token).build()

    # Add conversation handler with the states CHOOSING, TYPING_CHOICE and TYPING_REPLY
    conv_handler = ConversationHandler(
        entry_points=[CommandHandler("start", start)],
        states={
            START: [ # First, the user is asked to select the category of the info they want to learn
                CommandHandler("bitcoin", bitcoin),
                CommandHandler("ethereum", ethereum),
                CommandHandler("create_account", create_account),
                CommandHandler("login", login),
            ],
            LOGIN: [
                CommandHandler("account_info", account_info),
                CommandHandler("hyperopting", hyperopting),
                CommandHandler("config",config)
            ],
            CONFIG:[
                CommandHandler("change_strategy", change_strategy),
                CommandHandler("change_hyperopt_loss", change_hyperopt_loss),
                CommandHandler("change_hyperoptepochs", change_hyperoptepochs),
                CommandHandler("change_hyperopt_timeframe", change_hyperopt_timeframe),
                CommandHandler("change_hyperopt_timerange_days", change_hyperopt_timerange_days),
                CommandHandler("back", back_to_login),
            ],
            CHANGE_STRATEGY: [
                CommandHandler("cancel", cancel),
                MessageHandler(filters.Text(), change_strategy2)
            ],
            CHANGE_HYPEROPT_LOSS: [
                CommandHandler("cancel", cancel),
                MessageHandler(filters.Text(), change_hyperopt_loss2)
            ],
            CHANGE_EPOCHS: [
                CommandHandler("cancel", cancel),
                MessageHandler(filters.Text(), change_hyperoptepochs2)
            ],
            CHANGE_TIMEFRAME: [
                CommandHandler("cancel", cancel),
                MessageHandler(filters.Text(), change_hyperopt_timeframe2)
            ],
            CHANGE_TIMERANGE_DAYS: [
                CommandHandler("cancel", cancel),
                MessageHandler(filters.Text(), change_hyperopt_timerange2)
            ],
            
        },
        fallbacks=[CommandHandler("cancel", cancel)],
    )

    application.add_handler(conv_handler)

    # Run the bot until the user presses Ctrl-C
    application.run_polling()


if __name__ == "__main__":
    main()