# Freqtrade Controller

## Getting started

Start with /login

## Hyperopting

You can with /hyperopting search for the best strategy.

Start with /config to change your strategy
then /hyperopting to start the hyperopt
for search the best strategy

When you started /hyperopting then wait for the result
You cant start /hyperopting again bevor the other is finish


## Config

### Change the Strategy
    With witch strategy you want to trade

### Change Hyperopt Loss Function
    With witch Hyperopt Loss Function you want to hyperopt

### Change Hyperopt Epochs
    How much epochs you want to run the hyperopt

### Change Hyperopt Timeframe
    With witch timeframe you want to trade

### Change Hyperopt Timerange Days

    How long the strategy should look back in days

